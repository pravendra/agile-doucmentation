AMP(Accelerated Mobile Page)
-----

Navigate to Link [AMP](https://www.ampproject.org/) for theory part.
-----
 
# AMP HTML Rules and tags Description

| Rules | Description |
| --------- | ------------ |
| Start with the `<!doctype html>` doctype. |  Standard for HTML. |
| Contain a top-level `<html amp>` tag (`<html amp>` is accepted as well).| Identifies the page as AMP content. |
| Contain `<head>` and `<body>` tags. | Optional in HTML but not in AMP. |
| Contain a `<meta charset="utf-8">` tag as the first child of their `<head>` tag. | Identifies the encoding for the page. |
| Contain a `<script async src="https://cdn.ampproject.org/v0.js"></script>` tag as the second child of their `<head>` tag.| Includes and loads the AMP JS library. |
| Contain a `<link rel="canonical" href="$SOME_URL">` tag inside their `<head>` | Points to the regular HTML version of the AMP HTML document or to itself if no such HTML version exists. Learn more in [Make Your Page Discoverable.](https://www.ampproject.org/docs/guides/discovery)|
| Contain a `<meta name="viewport" content="width=device-width,minimum-scale=1">` tag inside their `<head>` tag. It's also recommended to include `initial-scale=1`. | Specifies a responsive viewport. Learn more in [Create Responsive AMP Pages](https://www.ampproject.org/docs/guides/responsive/responsive_design). |
| Contain the [AMP boilerplate code](https://www.ampproject.org/docs/reference/spec/amp-boilerplate) in their `<head>` tag. | CSS boilerplate to initially hide the content until AMP JS is loaded. |

# Steps to write AMP HTML

- Create AMP page using above tags
- Include `cdn` link in head section for each tag in AMP because AMP has its own Validation and JavaScript.
`If not included, it will throw error`.
- Write AMP tag for example `<amp-img>` instead of `<img>` tags of html.
- Check for validation successfull in console 



# Check AMP Validation

To Check For Amp Validation Goto tools>> Developer tools
or right click and then inspect element

Then Write `#development=1` in the URL and reload the page again.

AMP Validation Successfull should appear in the console. 

You can also type the code in [AMP-Playground](https://ampbyexample.com/playground/) to check AMP VALIDATION SUCCESSFULL

# Simple AMP Coding Example 

First HTML `<tags>` are replaced by `<amp-tag>`

Some tags like `<div>`, `<span>`, `<li>` tags are used as it is. 

##### For more example click in on the link __[More Example](https://ampbyexample.com/)__

 Images, Videos , Forms and How to include Custom CSS is shown in below example.


#### To include Image in AMP
````
    <amp-img alt="Mountains"
    width="550"
     height="368"
    src="/img/flower.png">
    <amp-img alt="Mountains"
    fallback
    width="550"
    height="368"
    src="/img/flower.png"></amp-img>
    </amp-img> 
 `````
 
####  To include youtube video in AMP
 
 You can get videoID from url after `?` mark.
 ````
 <head>
    <script async custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"></script>
 </head>

<body>
    <amp-youtube width="480"
    height="270"
    data-videoid="lBTCB7yLs8Y">
    </amp-youtube>
</body>
</html>
````
#### To include normal video  in AMP

````
<head>
    <script async custom-element="amp-video" src="https://cdn.ampproject.org/v0/amp-video-0.1.js"></script>
</head>

<body>
    <amp-video width="480"
    height="270"
    src="/video/tokyo.mp4"
    poster="/img/tokyo.jpg"
    layout="responsive"
    controls>
    <div fallback>
    <p>Your browser doesn't support HTML5 video.</p>
    </div>
    <source type="video/mp4"
    src="/video/tokyo.mp4">
    <source type="video/webm"
    src="/video/tokyo.webm">
    </amp-video>
</body>

`````

#### To include Custom css in AMP

`````
<head>
    <style amp-custom>
        h1 {
        margin: 16px;
        text-align : center;
        }
    </style>
</head>

<body>
  <h1>Hello!! Welcome to AMP Page </h1>
</body>
`````

#### To include Form in AMP

`````
<head>
    <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>

    <style amp-custom>
    form.amp-form-submit-success [submit-success],
    form.amp-form-submit-error [submit-error]{
    margin-top: 16px;
    }
    form.amp-form-submit-success [submit-success] {
    color: green;
    }
    form.amp-form-submit-error [submit-error] {
    color: red;
    }
    form.amp-form-submit-success.hide-inputs > input {
    display: none;
    }
    </style>
</head>
<body>
<form method="post"
  class="p2"
  action-xhr="/components/amp-form/submit-form-input-text-xhr"
  target="_top">
  <div class="ampstart-input inline-block relative m0 p0 mb3">
    <input type="text"
      class="block border-none p0 m0"
      name="name"
      placeholder="Name..."
      required>
    <input type="email"
      class="block border-none p0 m0"
      name="email"
      placeholder="Email..."
      required>
  </div>
  <input type="submit"
    value="Subscribe"
    class="ampstart-btn caps">
  <div submit-success>
    <template type="amp-mustache">
      Success! Thanks {{name}} for trying the
      <code>amp-form</code> demo! Try to insert the word "error" as a name input in the form to see how
      <code>amp-form</code> handles errors.
    </template>
  </div>
  <div submit-error>
    <template type="amp-mustache">
      Error! Thanks {{name}} for trying the
      <code>amp-form</code> demo with an error response.
    </template>
  </div>
</form>
</body>

`````
#### To include Iframe in AMP

The major problem in AMP is to include our own JavaScript.We can do it with the help of iframe - `<amp-iframe>`.

- __How to declare `Iframe`__ 
  
  `Iframe` should 600px or 75% from top and cannot display on the top until and unless `placeholder` is used.

  An `amp-iframe` must only request resources via HTTPS, from a data-URI, or via the srcdoc attribute.

  An amp-iframe must not be in the same origin as the container unless they do not allow `allow-same-origin` in the sandbox attribute.


# Steps for including current Form page of AEM in AMP

- Create a Project in AEM and with the help of the `form` store the dat in specified storage.

  Data will be stored with the help of sevlet and same data will be diplayed below the `form` with the help of ajax calling

-  Then go to `localhost:4502/aem/inbox` and then click on `Configure HTTPS`.

- Enter the `Key Store Password` and `Trust Store Password` and then click on the `next` button.

- Upload the `Key & Certificate file` and then click on the `next` button. How to create `Key & Certifcate file` is documented on the link [SSL Wizard in AEM](https://helpx.adobe.com/experience-manager/kt/platform-repository/using/ssl-wizard-technical-video-use.html)

- Specify the port and click on the `next`.

- Your AEM will open on the https://localhost:8443/aem/start.html.

- Now open your project in AEM on the above `host id` and copy the url.

- Now Open the AEM in http://localhost4502 and create a project in AEM using `amp-iframe`

- Specify the copied url in the `amp-iframe` src attribute.

- Now see the output in http://localhost4502/content/`your url`.

- Avoid seeing the output in http://localhost4502/editor/content/`your url`. 

- Please find `amp-iframe` coding part below.

`````
<head>

  <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>

</head>

<body>
<amp-iframe width="500"
      title="Resizable iframe example from 200x200 to 300x300 "
      height="500"
      sandbox="allow-scripts allow-same-origin allow-forms"
      allowfullscreen
      layout="responsive"
      frameborder="0"
      src="https://localhost:8443/content/ecommtest/training.html"
      class="m1">
      <amp-img layout="fill"
      src="https://encrypted-tbn0.gstatic.com/images? q=tbn:ANd9GcRpK9S83oFEQpl_2evjy6GvqZ85d4LNz0xwJxcsJdnV-roDvwXP9Q"
    placeholder></amp-img>
    <div overflow
    tabindex="0"
    role="button"
    class="ampstart-card py1"
    aria-label="Show more">Click to show more</div>
</amp-iframe>
</body>
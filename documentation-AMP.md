
# AMP (Accelerated Mobile Page)


## What is AMP?
 The __[Accelerated Mobile Pages](https://www.ampproject.org/)__ (AMP) is an open-source initiative to improve the performance of web content and advertisements through a publishing technology known as AMP.

 Technically speaking, AMP meaning is an open-source coding standard which relies on a simplified version of HTML to produce a more basic version of your website which can load much more rapidly.

# Technology 




### Open Web Format

 AMP pages are published on the open web and can be displayed in most current browsers.When a standard webpage has an AMP counterpart, a link to the AMP page is usually placed in an HTML tag in the source code of the standard page. Because most AMP pages are easily discoverable by __web crawlers__, third parties such as search engines and other referring websites can choose to link to the AMP version of a webpage instead of the standard version.


### AMP framework


The AMP framework consists of three components: AMP HTML which is a standard HTML with web components; AMP JavaScript which manages resource loading; and AMP caches which can serve and validate AMP pages.

Most AMP pages are delivered by Google’s AMP cache, but other companies can support AMP caches. Internet performance and security company Cloudflare launched an AMP cache in March 2017.



### Performance


Google reports that AMP pages served in Google search typically load in less than one second and use 10 times less data than the equivalent non-AMP pages.CNBC reported a 387% decrease in mobile page load time for AMP Pages over non-AMP pages, while Gizmodo reported that AMP pages loaded three times faster than non-AMP pages

# Three Core Components Of AMP



- ##  AMP HTML
  
     AMP HTML is HTML with some restrictions for reliable performance.AMP HTML is basically HTML extended with custom AMP properties. The simplest AMP HTML file looks like this:

```
<!doctype html>
<html amp>
 <head>
   <meta charset="utf-8">
   <link rel="canonical" href="hello-world.html">
   <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
   <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
   <script async src="https://cdn.ampproject.org/v0.js"></script>
 </head>
 <body>Hello World!</body>
</html>
```

 Though most tags in an AMP HTML page are regular HTML tags, some HTML tags are replaced with AMP-specific tags (see also [HTML Tags in the AMP spec](https://www.ampproject.org/docs/reference/spec)). These custom elements, called AMP HTML components, make common patterns easy to implement in a performant way.


- ##  AMP JS

     The AMP JS library ensures the fast rendering of AMP HTML pages.

     The AMP JS library implements all of AMP's best performance practices, manages resource loading and gives you the custom tags mentioned above, all to ensure a fast rendering of your page.

     Among the biggest optimizations is the fact that it makes everything that comes from external resources asynchronous, so nothing in the page can block anything from rendering.

     Other performance techniques include the sandboxing of all iframes, the pre-calculation of the layout of every element on page before resources are loaded and the disabling of slow CSS selectors


- ##  AMP Cache

     The Google AMP Cache can be used to serve cached AMP HTML pages.

     The Google AMP Cache is a proxy-based content delivery network for delivering all valid AMP documents. It fetches AMP HTML pages, caches them, and improves page performance automatically. When using the Google AMP Cache, the document, all JS files and all images load from the same origin that is using HTTP 2.0 for maximum efficiency.

     The cache also comes with a built-in validation system which confirms that the page is guaranteed to work, and that it doesn't depend on external resources. The validation system runs a series of assertions confirming the page’s markup meets the AMP HTML specification.

     Another version of the validator comes bundled with every AMP page. This version can log validation errors directly to the browser’s console when the page is rendered, allowing you to see how complex changes in your code might impact performance and user experience.

#  How AMP Works 

- ##  Execute all AMP JavaScript asynchronously

     JavaScript is powerful, it can modify just about every aspect of the page, but it can also block DOM construction and delay page rendering (see also Adding interactivity with JavaScript). To keep JavaScript from delaying page rendering, AMP allows only asynchronous JavaScript.
  
     AMP pages can’t include any author-written JavaScript. Instead of using JavaScript, interactive page features are handled in custom AMP elements. The custom AMP elements may have JavaScript under the hood, but they’re carefully designed to make sure they don’t cause performance degradation.
  
     While third-party JS is allowed in iframes, it cannot block rendering. For example, if third-party JS uses the super-bad-for-performance document.write API, it does not block rendering the main page.

- ## Size all resources statically

     External resources such as images, ads or iframes must state their size in the HTML so that AMP can determine each element’s size and position before resources are downloaded. AMP loads the layout of the page without waiting for any resources to download.
    
     AMP uncouples document layout from resource layout. Only one HTTP request is needed to layout the entire doc (+fonts). Since AMP is optimized to avoid expensive style recalculations and layouts in the browser, there won’t be any re-layout when resources load.

- ##  Don’t let extension mechanisms block rendering

     AMP doesn’t let extension mechanisms block page rendering. AMP supports extensions for things like `lightboxes`, `instagram embeds`, `tweets`, etc. While these require additional HTTP requests, those requests do not block page layout and rendering.

     Any page that uses a custom script must tell the AMP system that it will eventually have a custom tag. For example, the amp-iframe script tells the system that there will be an `amp-iframe` tag. AMP creates the iframe box before it even knows what it will include:
```
  <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
```

- ## Keep all third-party JavaScript out of the critical path
 
     Third-party JS likes to use synchronous JS loading. They also like to `document.write` more sync scripts. For example, if you have five ads on your page, and each of them cause three synchronous loads, each with a 1 second latency connection, you’re in 15 seconds of load time just for JS loading.

     AMP pages allow third-party JavaScript but only in sandboxed iframes. By restricting them to iframes, they can’t block the execution of the main page. Even if they trigger multiple style re-calculations, their tiny iframes have very little DOM.

     The time it takes to do style-recalculations and layouts are restricted by DOM size, so the iframe recalculations are very fast compared to recalculating styles and layout for the page.

- ## All CSS must be inline and size-bound

     CSS blocks all rendering, it blocks page load, and it tends to get bloated. In AMP HTML pages, only inline styles are allowed. This removes 1 or often more HTTP requests from the critical rendering path compared to most web pages.

     Also, the inline style sheet has a maximum size of 50 kilobytes. While this size is big enough for very sophisticated pages, it still requires the page author to practice good CSS hygiene.

- ## Minimize style recalculations

     Each time you measure something, it triggers style recalculations which are expensive because the browser has to layout the entire page. In AMP pages, all DOM reads happen first before all the writes. This ensures there’s the max of one recalc of styles per frame.

- ## Only run GPU-accelerated animations

     The only way to have fast optimizations is to run them on the GPU. GPU knows about layers, it knows how to perform some things on these layers, it can move them, it can fade them, but it can’t update the page layout; it will hand that task over to the browser, and that’s not good.

     The rules for animation-related CSS ensure that animations can be GPU-accelerated. Specifically, AMP only allows animating and transitioning on transform and opacity so that page layout isn’t required.
  
# AMP HTML Rules and tags Description

| Rules | Description |
| --------- | ------------ |
| Start with the `<!doctype html>` doctype. |  Standard for HTML. |
| Contain a top-level `<html amp>` tag (`<html amp>` is accepted as well).| Identifies the page as AMP content. |
| Contain `<head>` and `<body>` tags. | Optional in HTML but not in AMP. |
| Contain a `<meta charset="utf-8">` tag as the first child of their `<head>` tag. | Identifies the encoding for the page. |
| Contain a `<script async src="https://cdn.ampproject.org/v0.js"></script>` tag as the second child of their `<head>` tag.| Includes and loads the AMP JS library. |
| Contain a `<link rel="canonical" href="$SOME_URL">` tag inside their `<head>` | Points to the regular HTML version of the AMP HTML document or to itself if no such HTML version exists. Learn more in [Make Your Page Discoverable.](https://www.ampproject.org/docs/guides/discovery)|
| Contain a `<meta name="viewport" content="width=device-width,minimum-scale=1">` tag inside their `<head>` tag. It's also recommended to include `initial-scale=1`. | Specifies a responsive viewport. Learn more in [Create Responsive AMP Pages](https://www.ampproject.org/docs/guides/responsive/responsive_design). |
| Contain the [AMP boilerplate code](https://www.ampproject.org/docs/reference/spec/amp-boilerplate) in their `<head>` tag. | CSS boilerplate to initially hide the content until AMP JS is loaded. |

# Simple AMP Coding Example 

 For more example click in on the link __[More Example](https://ampbyexample.com/)__

 Images, Videos and How to include Custom CSS is shown in below example.
 ```
 
<!doctype html>
<html amp>
<head>
  
    <meta charset="utf-8">
  
    <link rel="canonical" href="self.html" />
  
    <meta name="viewport" content="width=device-width,minimum-scale=1">
  
    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>

    <script async custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"></script>

    <script async custom-element="amp-video" src="https://cdn.ampproject.org/v0/amp-video-0.1.js"></script>
  
    <script async src="https://cdn.ampproject.org/v0.js"></script>

    <style amp-custom>
        h1 {
        margin: 16px;
        text-align : center;
        }
    </style>
</head>
<body>
     <h1>Hello, AMP world!</h1><br>
  
    <amp-img alt="Mountains"
    width="550"
     height="368"
    src="/img/flower.png">
    <amp-img alt="Mountains"
    fallback
    width="550"
    height="368"
    src="/img/flower.png"></amp-img>
    </amp-img>
    <br>

    <amp-youtube width="480"
    height="270"
    data-videoid="lBTCB7yLs8Y">
    </amp-youtube>
</body>
</html>
 ```
